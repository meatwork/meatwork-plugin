package [(${packageName})];
import com.meatwork.tools.event.StartupManager;
import javax.annotation.processing.Generated;

@Generated("Meatwork Powered")
public class [(${className})] {

    private [(${className})]() {
        //private constructor
    }

    public static void register() {[# th:each="item : ${classesBinderToExecute}" #]
        [(${item})].INSTANCE.registerBind();[/]
    }

}