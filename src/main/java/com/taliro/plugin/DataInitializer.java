package com.taliro.plugin;

import com.github.javaparser.ast.CompilationUnit;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.ClassParser;
import com.taliro.plugin.scanner.ClassScanner;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * The type Data initializer.
 */
/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Getter
public class DataInitializer {

    /**
     * -- GETTER --
     *  Gets scan directory.
     *
     * @return the scan directory
     */
    private final File scanDirectory;
    /**
     * -- GETTER --
     *  Gets base package.
     *
     * @return the base package
     */
    private final String basePackage;
    /**
     * -- GETTER --
     *  Gets class finders event.
     *
     * @return the class finders event
     */
    private final List<ClassFinder> classFinders = new ArrayList<>();
    /**
     * -- GETTER --
     *  Gets class name event generated list.
     *
     * @return the class name event generated list
     */
    private final List<String> classNameEventGeneratedList = new ArrayList<>();
    /**
     * -- GETTER --
     *  Gets class name event from service generated list.
     *
     * @return the class name event from service generated list
     */
    private final List<String> classNameEventFromServiceGeneratedList = new ArrayList<>();
    /**
     * -- GETTER --
     *  Gets class name service generated list.
     *
     * @return the class name service generated list
     */
    private final List<String> classNameServiceGeneratedList = new ArrayList<>();
    /**
     * -- GETTER --
     *  Gets class name bind generated list.
     *
     * @return the class name bind generated list
     */
    private final List<String> classNameBindList = new ArrayList<>();
    /**
     * -- GETTER --
     *  Gets class router list.
     *
     * @return the class name router generated list
     */
    private final List<String> classRouterList = new ArrayList<>();
    /**
     * -- GETTER --
     *  Gets configurations.
     *
     * @return configurations meatwork list
     */
    private final Map<String, Object> configurations = new HashMap<>();

    private final Map<String, String> entityList = new HashMap<>();


    /**
     * Instantiates a new Data initializer.
     *
     * @param inputConfiguration the input configuration
     * @throws IOException the io exception
     */
    public DataInitializer(final InputConfiguration inputConfiguration) throws IOException {

        this.scanDirectory = inputConfiguration.scanDirectory();
        this.basePackage = inputConfiguration.basePackage();

        List<File> javaFiles = findJavaFiles(inputConfiguration.sourceDirectory());

        for (File javaFile : javaFiles) {
            CompilationUnit parse = new ClassScanner(javaFile.getAbsolutePath()).parse();
            ClassFinder classFinder = new ClassParser(parse).build();
            if(classFinder == null) continue;
            if (classFinder
                    .getType()
                    .equals(ClassFinder.TypeClass.CONFIGURATION)) {
                configurations.putAll(classFinder.getAnnotationFinded());
                continue;
            }
            classFinders.add(classFinder);
        }
    }


    /**
     * Add class name event.
     *
     * @param classNameEvent the class name event
     */
    public void addClassNameEvent(String classNameEvent) {
        this.classNameEventGeneratedList.add(classNameEvent);
    }

    /**
     * Add class name service.
     *
     * @param classNameService the class name service
     */
    public void addClassNameService(String classNameService) {
        this.classNameServiceGeneratedList.add(classNameService);
    }

    /**
     * Add class name bind.
     *
     * @param classNameService the class name service
     */
    public void addClassNameBind(String classNameService) {
        this.classNameBindList.add(classNameService);
    }

    /**
     * Add class name router.
     *
     * @param classRouter the class name service
     */
    public void addClassRouter(String classRouter) {
        this.classRouterList.add(classRouter);
    }

    /**
     * Add class entity.
     *
     * @param classEntity the class entity
     */
    public void addClassEntity(String classEntity, String basePackage) { this.entityList.put(classEntity, basePackage); }



    /**
     * Gets output directory.
     *
     * @return the output directory
     */
    public String getOutputDirectory() {
        return getScanDirectory()
                            .toPath()
                            .resolve(getBasePackage().replaceAll(
                                    Pattern.quote("."),
                                    "/"
                            ))
                            .toString();
    }

    private static List<File> findJavaFiles(File directory) {

        List<File> javaFiles = new ArrayList<>();
        if (directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        javaFiles.addAll(findJavaFiles(file));
                    } else if (file.getName().toLowerCase().endsWith(".java")) {
                        javaFiles.add(file);
                    }
                }
            }
        }

        return javaFiles;
    }
}
