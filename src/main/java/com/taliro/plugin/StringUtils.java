package com.taliro.plugin;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class StringUtils {

    private StringUtils() {}

    public static String capitalizeFirstLetter(String input) {
        if (input == null || input.isEmpty()) {
            return input;
        }
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }
}
