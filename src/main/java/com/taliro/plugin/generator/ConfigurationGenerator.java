package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.ConfigurationGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassName;
import java.io.IOException;

public class ConfigurationGenerator extends AbstractGenerator {

    private static final ClassName configurationLifeCycle = new ClassName("ConfigurationLifeCycle");

    public ConfigurationGenerator(DataInitializer dataInitializer) {
        super(dataInitializer);
    }

    @Override
    public void generate() throws IOException {
        TemplateGenerator.CONFIGURATION_LIFE_CYCLE.toFile(
            new ConfigurationGeneratorTemplate(
                this.dataInitializer.getBasePackage(),
                configurationLifeCycle.getFullname()
            ),
            this.dataInitializer.getOutputDirectory(),
            configurationLifeCycle.getFilename()
        );
    }
}
