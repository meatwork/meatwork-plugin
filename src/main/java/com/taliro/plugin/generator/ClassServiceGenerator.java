package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.ClassServiceGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.ClassName;
import com.taliro.plugin.scanner.factory.ElementType;
import com.taliro.plugin.scanner.factory.component.BindMethodFinder;
import com.taliro.plugin.scanner.factory.service.InjectMethodFinder;
import com.taliro.plugin.tools.Finder;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ClassServiceGenerator extends AbstractGenerator {
    public ClassServiceGenerator(DataInitializer dataInitializer) {
        super(dataInitializer);
    }

    @Override
    public void generate() throws IOException {
        List<ClassFinder> classFinderList = this.dataInitializer
                .getClassFinders()
                .stream()
                .filter(it -> it
                        .getType()
                        .equals(ClassFinder.TypeClass.SERVICE)
                )
                .toList();

        for (ClassFinder classFinder : classFinderList) {
            ClassName className = new ClassName(classFinder.getPackageName(), classFinder.getSimpleName(), "LifeCycle");

            InjectMethodFinder injectMethodFinder = classFinder
                    .getElementFinders()
                    .stream()
                    .filter(it -> it
                            .getElementType()
                            .equals(ElementType.INJECT))
                    .map(it -> (InjectMethodFinder) it)
                    .findFirst()
                    .orElse(null);

            String classesToInject = null;
            if (injectMethodFinder != null) {
                classesToInject = injectMethodFinder
                        .dependencies()
                        .stream()
                        .map(it -> Finder.getImplFromInterface(dataInitializer, it) + "LifeCycle.INSTANCE.getInstance()")
                        .collect(Collectors.joining(","));
            }

            List<BindMethodFinder> bindMethodFinderList = classFinder
                    .getElementFinders()
                    .stream()
                    .filter(it -> it.getElementType().equals(ElementType.BIND))
                    .map(it -> (BindMethodFinder) it)
                    .toList();

            if (!bindMethodFinderList.isEmpty()) {
                dataInitializer.addClassNameBind(className.getFullname());
            }

            TemplateGenerator.SERVICE_GENERATOR.toFile(
                    new ClassServiceGeneratorTemplate(
                            this.dataInitializer.getBasePackage(),
                            className.getFullname(),
                            className.getSimpleNameWithPackage(),
                            classesToInject,
                            classFinder.getInterfaceLinked(),
                            bindMethodFinderList
                    ),
                    this.dataInitializer.getOutputDirectory(),
                    className.getFilename()
            );

            this.dataInitializer.addClassNameService(className.getFullname());
        }
    }
}
