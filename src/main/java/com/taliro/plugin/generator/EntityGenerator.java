package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.EntityGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.ClassName;
import com.taliro.plugin.scanner.factory.entity.PropertyFinderEntity;
import com.taliro.plugin.scanner.factory.entity.parser.PropertyFinder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class EntityGenerator extends AbstractGenerator {

    public EntityGenerator(DataInitializer dataInitializer) {
        super(dataInitializer);
    }

    @Override
    public void generate() throws IOException {
        List<ClassFinder> entity =
            this.dataInitializer.getClassFinders()
                .stream()
                .filter(it -> it.getType().equals(ClassFinder.TypeClass.ENTITY))
                .toList();

        List<ClassFinder> entityParent =
            this.dataInitializer.getClassFinders()
                .stream()
                .filter(it -> it.getType().equals(ClassFinder.TypeClass.ENTITY_PARENT))
                .toList();

        for (ClassFinder entityCurrent : entity) {
            ClassName entityClassName = new ClassName(entityCurrent.getSimpleName(), "Entity");
            ClassName entityEnumClassName = new ClassName(
                entityCurrent.getPackageName(),
                entityCurrent.getSimpleName(),
                null
            );
            PropertyFinderEntity elementFinders = entityCurrent
                .getElementFinders()
                .stream()
                .map(it -> (PropertyFinderEntity) it)
                .findFirst()
                .orElseThrow(() ->
                    new IllegalArgumentException(
                        "cannot found PropertyFinderEntity on %s".formatted(entityCurrent.getSimpleName())
                    )
                );

            List<PropertyFinder> propertyFinderist = new ArrayList<>();

            if (elementFinders.propertyFinders() != null) {
	            propertyFinderist.addAll(elementFinders.propertyFinders());
            }

            if (elementFinders.entityLinked() != null) {
                List<ClassFinder> classFinderList = entityParent
                    .stream()
                    .filter(it -> elementFinders.entityLinked().contains(new ClassName(it.getPackageName(), it.getSimpleName(), null).getSimpleNameWithPackage()))
                    .toList();

                List<PropertyFinder> propertyFinders = classFinderList
                    .stream()
                    .flatMap(it -> it.getElementFinders().stream())
                    .flatMap(it -> ((PropertyFinderEntity) it).propertyFinders().stream())
                    .toList();

                propertyFinderist.addAll(propertyFinders);
            }

            PropertyFinder idFields = propertyFinderist
                    .stream()
                    .filter(PropertyFinder::isIdField)
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("cannot find id field"));

            this.dataInitializer.addClassEntity(entityClassName.getName(), entityCurrent.getPackageName());

            TemplateGenerator.ENTITY.toFile(
                new EntityGeneratorTemplate(
                    this.dataInitializer.getBasePackage(),
                    entityClassName.getFullname(),
                    entityEnumClassName.getSimpleNameWithPackage(),
                    idFields.fullClassName() + "." + idFields.propName(),
                    propertyFinderist
                ),
                this.dataInitializer.getOutputDirectory(),
                entityClassName.getFilename()
            );
        }
    }
}
