package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.StartupRegistryGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassName;

import java.io.IOException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class StartupRegistryGenerator extends AbstractGenerator{

	private static final ClassName startupRegistryClassName = new ClassName("StartupRegistry");

	public StartupRegistryGenerator(DataInitializer dataInitializer) {
		super(dataInitializer);
	}

	@Override
	public void generate() throws IOException {
		TemplateGenerator.STARTUP_REGISTRY.toFile(
				new StartupRegistryGeneratorTemplate(
						this.dataInitializer.getBasePackage(),
						startupRegistryClassName.getFullname(),
						this.dataInitializer.getClassNameEventFromServiceGeneratedList()
				),
				this.dataInitializer.getOutputDirectory(),
				startupRegistryClassName.getFilename()
		);
	}
}
