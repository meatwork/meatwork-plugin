package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.RouterRegistryGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassName;

import java.io.IOException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class RouterRegistryGenerator extends AbstractGenerator{

	private static final ClassName routerRegistryClassName = new ClassName("RouterRegistry");

	public RouterRegistryGenerator(DataInitializer dataInitializer) {
		super(dataInitializer);
	}

	@Override
	public void generate() throws IOException {
		TemplateGenerator.ROUTER_REGISTRY.toFile(
				new RouterRegistryGeneratorTemplate(
						this.dataInitializer.getBasePackage(),
						routerRegistryClassName.getFullname(),
						this.dataInitializer.getClassRouterList()
				),
				this.dataInitializer.getOutputDirectory(),
				routerRegistryClassName.getFilename()
		);
	}
}
