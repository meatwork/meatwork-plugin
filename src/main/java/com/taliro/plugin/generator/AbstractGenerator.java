package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;

import java.io.IOException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public abstract class AbstractGenerator {

    protected final DataInitializer dataInitializer;

    public AbstractGenerator(DataInitializer dataInitializer) {
        this.dataInitializer = dataInitializer;
    }

    public abstract void generate() throws IOException;

}
