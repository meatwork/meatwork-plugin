package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.ClassEventGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.ClassName;
import com.taliro.plugin.scanner.factory.ElementType;
import com.taliro.plugin.scanner.factory.component.BindMethodFinder;
import com.taliro.plugin.scanner.factory.component.ObserveMethodFinder;
import com.taliro.plugin.scanner.factory.component.PathMethodFinder;
import com.taliro.plugin.scanner.factory.component.StartupMethodFinder;
import com.taliro.plugin.scanner.factory.service.InjectMethodFinder;
import com.taliro.plugin.tools.Finder;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ClassComponentGenerator extends AbstractGenerator {

    public ClassComponentGenerator(DataInitializer dataInitializer) {
        super(dataInitializer);
    }

    public void generate() throws IOException {
        String basePackage = this.dataInitializer.getBasePackage();
        List<ClassFinder> classFindersEvent =
            this.dataInitializer.getClassFinders()
                .stream()
                .filter(it -> it.getType().equals(ClassFinder.TypeClass.COMPONENT))
                .toList();

        for (ClassFinder classFinder : classFindersEvent) {
            ClassName className = getClassName(classFinder);
            List<ObserveMethodFinder> methodObserve = classFinder
                .getElementFinders()
                .stream()
                .filter(it -> it.getElementType().equals(ElementType.OBSERVE))
                .map(it -> (ObserveMethodFinder) it)
                .toList();

            List<StartupMethodFinder> methodStartup = classFinder
                .getElementFinders()
                .stream()
                .filter(it -> it.getElementType().equals(ElementType.STARTUP))
                .map(it -> (StartupMethodFinder) it)
                .toList();

            List<PathMethodFinder> methodPath = classFinder
                .getElementFinders()
                .stream()
                .filter(it -> it.getElementType().equals(ElementType.PATH))
                .map(it -> (PathMethodFinder) it)
                .toList();

            InjectMethodFinder injectMethodFinder = classFinder
                .getElementFinders()
                .stream()
                .filter(it -> it.getElementType().equals(ElementType.INJECT))
                .map(it -> (InjectMethodFinder) it)
                .findFirst()
                .orElse(null);

            List<BindMethodFinder> bindMethodFinderList = classFinder
                .getElementFinders()
                .stream()
                .filter(it -> it.getElementType().equals(ElementType.BIND))
                .map(it -> (BindMethodFinder) it)
                .toList();

            if (!bindMethodFinderList.isEmpty()) {
                dataInitializer.addClassNameBind(className.getFullname());
            }

            String classesToInject = null;
            if (injectMethodFinder != null) {
                classesToInject =
                    injectMethodFinder
                        .dependencies()
                        .stream()
                        .map(it ->
                            Finder.getImplFromInterface(dataInitializer, it) +
                            "LifeCycle.INSTANCE.getInstance()"
                        )
                        .collect(Collectors.joining(","));
            }

            if (!methodPath.isEmpty()) {
                this.dataInitializer.addClassRouter(className.getFullname());
            }

            TemplateGenerator.CLASS_COMPONENT_GENERATOR.toFile(
                new ClassEventGeneratorTemplate(
                    basePackage,
                    className.getFullname(),
                    classFinder.getPackageName() + "." + classFinder.getSimpleName(),
                    methodObserve,
                    methodStartup,
                    methodPath,
                    bindMethodFinderList,
                    classesToInject
                ),
                this.dataInitializer.getOutputDirectory(),
                className.getFilename()
            );

            // Save Classname
            this.dataInitializer.addClassNameEvent(className.getFullname());
            this.dataInitializer.addClassNameService(className.getFullname());
        }
    }

    private ClassName getClassName(ClassFinder classFinder) {
        return new ClassName(classFinder.getSimpleName(), "LifeCycle");
    }
}
