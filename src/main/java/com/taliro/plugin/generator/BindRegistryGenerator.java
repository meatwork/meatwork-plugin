package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.BindRegistryGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassName;

import java.io.IOException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class BindRegistryGenerator extends AbstractGenerator{
	private static final ClassName bindRegistryClassName = new ClassName("BindRegistry");

	public BindRegistryGenerator(DataInitializer dataInitializer) {
		super(dataInitializer);
	}

	@Override
	public void generate() throws IOException {
		TemplateGenerator.BIND_REGISTRY.toFile(
				new BindRegistryGeneratorTemplate(
						this.dataInitializer.getBasePackage(),
						bindRegistryClassName.getFullname(),
						this.dataInitializer.getClassNameBindList()
				),
				this.dataInitializer.getOutputDirectory(),
				bindRegistryClassName.getFilename()
		);
	}
}
