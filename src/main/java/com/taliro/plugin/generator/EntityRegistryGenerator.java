package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.EntityRegistryGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassName;

import java.io.IOException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class EntityRegistryGenerator extends AbstractGenerator{

	private static final ClassName entityRegistryClassName = new ClassName("EntityRegistry");

	public EntityRegistryGenerator(DataInitializer dataInitializer) {
		super(dataInitializer);
	}

	@Override
	public void generate() throws IOException {
		TemplateGenerator.ENTITY_REGISTRY.toFile(
				new EntityRegistryGeneratorTemplate(
						this.dataInitializer.getBasePackage(),
						entityRegistryClassName.getFullname(),
						this.dataInitializer.getEntityList()
				),
				this.dataInitializer.getOutputDirectory(),
				entityRegistryClassName.getFilename()
		);
	}
}
