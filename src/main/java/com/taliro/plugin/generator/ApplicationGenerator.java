package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.ApplicationGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;

import java.io.IOException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ApplicationGenerator extends AbstractGenerator {

	public ApplicationGenerator(DataInitializer dataInitializer) {
		super(dataInitializer);
	}

	@Override
	public void generate() throws IOException {
		TemplateGenerator.APPLICATION.toFile(
				new ApplicationGeneratorTemplate(
						this.dataInitializer.getBasePackage(),
						dataInitializer.getConfigurations()
				),
				this.dataInitializer.getOutputDirectory(),
				"Application.java"
		);
	}
}
