package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.EntityRepositoryGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.ClassName;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class EntityRepositoryGenerator extends AbstractGenerator {

    public EntityRepositoryGenerator(DataInitializer dataInitializer) {
        super(dataInitializer);
    }

    @Override
    public void generate() throws IOException {
        Set<ClassFinder> classEntity =
            this.dataInitializer.getClassFinders()
                .stream()
                .filter(it -> it.getType().equals(ClassFinder.TypeClass.ENTITY))
                .collect(Collectors.toSet());

        for (ClassFinder classFinder : classEntity) {
            ClassName classNameRepository = new ClassName(
                classFinder.getPackageName(),
                classFinder.getSimpleName(),
                "Repository"
            );

            TemplateGenerator.ENTITY_REPOSITORY.toFile(
                new EntityRepositoryGeneratorTemplate(
                    this.dataInitializer.getBasePackage(),
                    classNameRepository.getFullname(),
                    new ClassName(classFinder.getPackageName(), classFinder.getSimpleName(), null)
                        .getName(),
                    new ClassName(
                        classFinder.getSimpleName(),
                        "Entity"
                    )
                        .getNameWithSuffix()
                ),
                this.dataInitializer.getOutputDirectory(),
                classNameRepository.getFilename()
            );
        }
    }
}
