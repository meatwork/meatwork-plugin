package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.ServiceRegistryGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassName;

import java.io.IOException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ServiceRegistryGenerator extends AbstractGenerator{

	private static final ClassName serviceRegistryClassName = new ClassName("ServiceRegistry");

	public ServiceRegistryGenerator(DataInitializer dataInitializer) {
		super(dataInitializer);
	}

	@Override
	public void generate() throws IOException {
		TemplateGenerator.SERVICE_REGISTRY.toFile(
				new ServiceRegistryGeneratorTemplate(
						this.dataInitializer.getBasePackage(),
						serviceRegistryClassName.getFullname(),
						this.dataInitializer.getClassNameServiceGeneratedList()
				),
				this.dataInitializer.getOutputDirectory(),
				serviceRegistryClassName.getFilename()
		);
	}
}
