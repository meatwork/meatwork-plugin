package com.taliro.plugin.generator;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.pojo.EventRegistryGeneratorTemplate;
import com.taliro.plugin.pojo.TemplateGenerator;
import com.taliro.plugin.scanner.ClassName;
import java.io.IOException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class EventRegistryGenerator extends AbstractGenerator {

    public EventRegistryGenerator(DataInitializer dataInitializer) {
        super(dataInitializer);
    }

    private static final ClassName eventRegistryClassName = new ClassName("EventRegistry");

    @Override
    public void generate() throws IOException {
        TemplateGenerator.EVENT_REGISTRY.toFile(
            new EventRegistryGeneratorTemplate(
                this.dataInitializer.getBasePackage(),
                eventRegistryClassName.getFullname(),
                this.dataInitializer.getClassNameEventGeneratedList()
            ),
            this.dataInitializer.getOutputDirectory(),
            eventRegistryClassName.getFilename()
        );
    }
}
