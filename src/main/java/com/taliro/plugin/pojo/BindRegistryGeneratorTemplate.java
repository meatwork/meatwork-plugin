package com.taliro.plugin.pojo;

import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record BindRegistryGeneratorTemplate(
		String packageName,
		String className,
		List<String> classesBinderToExecute
) {
}
