package com.taliro.plugin.pojo;

import com.taliro.plugin.scanner.factory.component.BindMethodFinder;
import com.taliro.plugin.scanner.factory.component.ObserveMethodFinder;
import com.taliro.plugin.scanner.factory.component.PathMethodFinder;
import com.taliro.plugin.scanner.factory.component.StartupMethodFinder;
import java.util.List;

public record ClassEventGeneratorTemplate(
    String packageName,
    String className,
    String fullNameClassToTarget,
    List<ObserveMethodFinder> classesToRegister,
    List<StartupMethodFinder> startupRegister,
    List<PathMethodFinder> routerRegister,
    List<BindMethodFinder> bindRegister,
    String classesToInject
)
    implements Template {}
