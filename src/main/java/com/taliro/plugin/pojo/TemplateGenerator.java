package com.taliro.plugin.pojo;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.taliro.plugin.FileUtils;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Map;

public class TemplateGenerator<T> {


    public static TemplateGenerator<ClassEventGeneratorTemplate> CLASS_COMPONENT_GENERATOR = new TemplateGenerator<>("ClassComponentGenerator.txt");
    public static TemplateGenerator<ApplicationGeneratorTemplate> APPLICATION = new TemplateGenerator<>("Application.txt");
    public static TemplateGenerator<ConfigurationGeneratorTemplate> CONFIGURATION_LIFE_CYCLE = new TemplateGenerator<>("ConfigurationLifeCycle.txt");
    public static TemplateGenerator<ClassServiceGeneratorTemplate> SERVICE_GENERATOR = new TemplateGenerator<>("ServiceGenerator.txt");
    public static TemplateGenerator<EntityGeneratorTemplate> ENTITY = new TemplateGenerator<>("Entity.txt");
    public static TemplateGenerator<BindRegistryGeneratorTemplate> BIND_REGISTRY = new TemplateGenerator<>("BindRegistry.txt");
    public static TemplateGenerator<EntityRegistryGeneratorTemplate> ENTITY_REGISTRY = new TemplateGenerator<>("EntityRegistry.txt");
    public static TemplateGenerator<EventRegistryGeneratorTemplate> EVENT_REGISTRY = new TemplateGenerator<>("EventRegistry.txt");
    public static TemplateGenerator<RouterRegistryGeneratorTemplate> ROUTER_REGISTRY = new TemplateGenerator<>("RouterRegistry.txt");
    public static TemplateGenerator<ServiceRegistryGeneratorTemplate> SERVICE_REGISTRY = new TemplateGenerator<>("ServiceRegistry.txt");
    public static TemplateGenerator<StartupRegistryGeneratorTemplate> STARTUP_REGISTRY = new TemplateGenerator<>("StartupRegistry.txt");
    public static TemplateGenerator<EntityRepositoryGeneratorTemplate> ENTITY_REPOSITORY = new TemplateGenerator<>("EntityRepository.txt");

    private final String template;
    private TemplateGenerator(String template) {
        this.template = template;
    }

    public void toFile(T data,
                String outputDirectory,
                String filename) throws IOException {
        ITemplateEngine templateEngine = buildTemplateEngine();
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, Object> map = null;

        if (data != null) {
            map = objectMapper
                    .convertValue(
                            data,
                            new TypeReference<>() {}
                    );
        }

        if (!FileUtils.hasFolderExistOrCreateIt(outputDirectory)) {
            throw new RuntimeException("cannot create folder %s".formatted(outputDirectory));
        }

        try {
            templateEngine.process(
                    "/templates/" + template,
                    new Context(
                            Locale.FRANCE,
                            map
                    ),
                    new FileWriter(Paths
                                           .get(outputDirectory)
                                           .resolve(filename)
                                           .toFile())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ITemplateEngine buildTemplateEngine() {
        final ClassLoaderTemplateResolver templateResolver =
                new ClassLoaderTemplateResolver();
        templateResolver.setTemplateMode(TemplateMode.TEXT);
        templateResolver.setSuffix(".java");
        templateResolver.setCacheTTLMs(3600000L);
        templateResolver.setCacheable(false);
        final TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        return templateEngine;

    }
}
