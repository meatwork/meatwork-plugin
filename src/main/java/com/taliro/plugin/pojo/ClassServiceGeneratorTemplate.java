package com.taliro.plugin.pojo;

import com.taliro.plugin.scanner.factory.component.BindMethodFinder;

import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record ClassServiceGeneratorTemplate(
    String packageName,
    String className,
    String classNameTarget,
    String classesToInject,
    String interfaceLinkedName,
    List<BindMethodFinder> bindRegister
) {}
