package com.taliro.plugin.pojo;

import com.meatwork.tools.configuration.MeatworkConfigurationType;

import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record ApplicationGeneratorTemplate(String packageName, Map<String, Object> configurations) {

	public boolean isEnableHttpServer() {
		return configurations.containsKey(MeatworkConfigurationType.ENABLE_HTTP_SERVER.name()) &&
				configurations.get(MeatworkConfigurationType.ENABLE_HTTP_SERVER.name()) == Boolean.TRUE;
	}

	public int getPortHttp() {
		Object o = configurations.get(MeatworkConfigurationType.PORT_HTTP_SERVER.name());
		if (o == null) {
			return 0;
		}
		return (int) o;
	}

	public boolean isEnableEntity() {
		return configurations.containsKey(MeatworkConfigurationType.ENABLE_ENTITY.name()) &&
				configurations.get(MeatworkConfigurationType.ENABLE_ENTITY.name()) == Boolean.TRUE;
	}

}
