package com.taliro.plugin.pojo;

import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record EntityRegistryGeneratorTemplate(
		String packageName,
		String className,
		Map<String, String> entityGeneratedMap
) {
}
