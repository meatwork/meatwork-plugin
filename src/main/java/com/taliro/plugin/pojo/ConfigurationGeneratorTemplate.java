package com.taliro.plugin.pojo;

public record ConfigurationGeneratorTemplate(
    String packageName,
    String className
) {}
