package com.taliro.plugin.pojo;

import com.taliro.plugin.scanner.factory.entity.parser.PropertyFinder;

import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record EntityGeneratorTemplate(
	String packageName,
	String className,
	String classNameEntity,
	String idField,
	List<PropertyFinder> propertyFinders
) {

}
