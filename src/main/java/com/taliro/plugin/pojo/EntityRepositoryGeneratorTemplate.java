package com.taliro.plugin.pojo;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record EntityRepositoryGeneratorTemplate(
		String packageName,
		String className,
		String classNameTarget,
		String classNameEntity
) {
}
