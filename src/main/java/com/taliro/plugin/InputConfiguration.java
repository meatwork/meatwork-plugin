package com.taliro.plugin;

import java.io.File;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record InputConfiguration(File sourceDirectory, File scanDirectory, String basePackage) {}
