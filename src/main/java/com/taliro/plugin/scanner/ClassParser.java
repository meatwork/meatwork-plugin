package com.taliro.plugin.scanner;

import com.github.javaparser.ast.CompilationUnit;
import com.taliro.plugin.scanner.factory.TypeClass;
import com.taliro.plugin.scanner.factory.TypeClassFactory;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public final class ClassParser {

    private final CompilationUnit compilationUnit;

    public ClassFinder build() {
        TypeClass typeClass = TypeClassFactory.getTypeClass(compilationUnit);

        if (typeClass == null) {
            return null;
        }

        return typeClass.getClassFinder();
    }
}
