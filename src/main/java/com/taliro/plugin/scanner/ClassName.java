package com.taliro.plugin.scanner;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

import lombok.Getter;

import java.util.Optional;

public class ClassName {

    @Getter
    private final String packageName;
    private final String name;
    private final String suffix;

    public ClassName(String name, String suffix) {
        this.packageName = "";
        this.name = name;
        this.suffix = suffix;
    }

    public ClassName(String packageName, String name, String suffix) {
        this.packageName = packageName;
        this.name = name;
        this.suffix = suffix;
    }

    public ClassName(String name) {
        this.packageName = "";
        this.name = name;
        this.suffix = "";
    }

    public String getFullname() {
        return "%s%s".formatted(name, suffix);
    }

    public String getNameWithSuffix() {
        return "%s%s".formatted(
                getName(),
                suffix
        );
    }

    public String getSimpleNameWithPackage() {
        return Optional
                .ofNullable(this.packageName)
                .map(it -> "%s.%s".formatted(
                        it,
                        getSimplename()
                ))
                .orElse(getSimplename());
    }

    public String getSimplename() {
        return this.name;
    }

    public String getName() {
        if (packageName == null || packageName.isEmpty()) {
            return this.name;
        }
        return "%s.%s".formatted(this.packageName, this.name);
    }

    public String getFilename() {
        return "%s.java".formatted(getFullname());
    }
}
