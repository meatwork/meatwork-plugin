package com.taliro.plugin.scanner.factory.service.parse;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class HttpParams {

	private final List<String> paramsNamed;

	public HttpParams(String path) {
		this.paramsNamed = getParameters(path);
	}

	private List<String> getParameters(String path) {
		List<String> list = new ArrayList<>();
		Pattern compile = Pattern.compile("\\[[a-zA-Z]*\\]");
		Matcher matcher = compile.matcher(path);
		if (matcher.find()) {
			for (int i = 0; i < matcher.groupCount(); i++) {
				list.add(matcher.group(i));
			}
		}
		return list;
	}
}
