package com.taliro.plugin.scanner.factory.service;

import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.ElementType;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record StartupMethodFinder(String methodName, String fullNameClassToTarget, int priority) implements ElementFinder {
    @Override
    public ElementType getElementType() {
        return ElementType.STARTUP;
    }
}
