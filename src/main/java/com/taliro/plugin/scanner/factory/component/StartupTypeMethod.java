package com.taliro.plugin.scanner.factory.component;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.meatwork.tools.service.Startup;
import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.TypeMethod;
import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class StartupTypeMethod implements TypeMethod {

    private final MethodDeclaration methodDeclaration;
    private final String fullCurrentClassNameString;

    public StartupTypeMethod(
        MethodDeclaration methodDeclaration,
        String fullCurrentClassNameString
    ) {
        this.methodDeclaration = methodDeclaration;
        this.fullCurrentClassNameString = fullCurrentClassNameString;
    }

    @Override
    public ElementFinder getMethodFinder() {
        int priority = 100;
        List<Node> nodes = methodDeclaration
            .getAnnotationByClass(Startup.class)
            .map(AnnotationExpr::getChildNodes)
            .orElse(null);

        if (nodes != null && nodes.size() == 2) {
            Node priorityValue = nodes.get(1);
            priority =
                Integer.parseInt(
                    ((IntegerLiteralExpr) ((MemberValuePair) priorityValue).getValue()).getValue()
                );
        }

        return new StartupMethodFinder(
            methodDeclaration.getName().getIdentifier(),
            fullCurrentClassNameString,
            priority
        );
    }
}
