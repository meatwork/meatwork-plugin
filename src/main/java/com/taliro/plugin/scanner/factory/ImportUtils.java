package com.taliro.plugin.scanner.factory;

import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.NodeList;

import java.util.Optional;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ImportUtils {

    private ImportUtils() {}

    public static String getFullNameByClassName(String nameClass, NodeList<ImportDeclaration> imports, String packageName) {
        return imports
            .stream()
            .filter(it -> it
		            .getName().toString().endsWith(nameClass))
            .map(it -> it
		            .getName().toString())
            .findFirst()
            .orElse(Optional.ofNullable(packageName).map(it -> it + "." + nameClass).orElse(nameClass));
    }

    public static String getFullNameByClassName(String nameClass, NodeList<ImportDeclaration> imports) {
        return getFullNameByClassName(nameClass, imports, null).trim();
    }
}
