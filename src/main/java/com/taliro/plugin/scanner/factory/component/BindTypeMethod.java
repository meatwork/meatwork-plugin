package com.taliro.plugin.scanner.factory.component;

import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.taliro.plugin.scanner.ClassName;
import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.ImportUtils;
import com.taliro.plugin.scanner.factory.TypeMethod;

import java.util.Objects;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class BindTypeMethod implements TypeMethod {

    private final MethodDeclaration methodDeclaration;
    private final String fullCurrentClassName;
    private final NodeList<ImportDeclaration> imports;
    private final String packageName;

    public BindTypeMethod(MethodDeclaration methodDeclaration, String fullCurrentClassName, NodeList<ImportDeclaration> imports, String packageName) {
        this.methodDeclaration = methodDeclaration;
        this.fullCurrentClassName = fullCurrentClassName;
        this.imports = imports;
        this.packageName = packageName;
    }

    @Override
    public ElementFinder getMethodFinder() {
        final Parameter parameter = methodDeclaration.getParameter(0);
        if (methodDeclaration.getParameters().size() != 1) {
            throw new IllegalArgumentException(
                "parameter for @bind must be one parameter ! %s#%s".formatted(
                        fullCurrentClassName,
                        methodDeclaration.getName().toString()
                    )
            );
        }

        if (!Objects.equals(
		        methodDeclaration.getTypeAsString(),
		        "void"
        )) {
            throw new IllegalArgumentException(
                    "@bind must be return void type ! %s#%s".formatted(
                            fullCurrentClassName,
                            methodDeclaration.getName().toString()
                    )
            );
        }

        final String nameClass = parameter.getTypeAsString();

        String importClass = ImportUtils.getFullNameByClassName(nameClass, imports, packageName);

        if (importClass == null) {
            importClass = new ClassName(packageName, nameClass, null).getName();
        }

        return new BindMethodFinder(methodDeclaration.getName().toString(), importClass);
    }
}
