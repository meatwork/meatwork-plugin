package com.taliro.plugin.scanner.factory.entity.parser;

import java.util.Arrays;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record PropertyFinder(String propName, String fullClassName, boolean isIdField, String classTypeName) {
    public String getPropNameToCamelCase() {
        String[] propNameSplit = propName.split("_");
        return Arrays.stream(propNameSplit).map(it -> {
            String substring = it.substring(
                    0,
                    1
            );
            return substring.toUpperCase() + it.substring(1).toLowerCase();
        }).collect(Collectors.joining());
    }

    public String getPropNameToLowerCamelCase() {
        String propNameToCamelCase = getPropNameToCamelCase();
        return propNameToCamelCase.substring(0, 1).toLowerCase() + propNameToCamelCase.substring(1);
    }
}
