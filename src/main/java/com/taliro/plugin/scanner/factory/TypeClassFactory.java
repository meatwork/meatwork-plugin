package com.taliro.plugin.scanner.factory;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.meatwork.tools.configuration.MeatworkConfiguration;
import com.meatwork.tools.event.Component;
import com.meatwork.tools.http.Path;
import com.meatwork.tools.model.Property;
import com.meatwork.tools.service.Service;
import com.taliro.plugin.scanner.factory.component.ComponentTypeClass;
import com.taliro.plugin.scanner.factory.configuration.MeatworkConfigurationTypeClass;
import com.taliro.plugin.scanner.factory.entity.ModelEntityTypeClass;
import com.taliro.plugin.scanner.factory.service.ServiceTypeClass;

import java.util.Optional;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class TypeClassFactory {

    public static final String PROPERTY_SIMPLE_NAME = Property.class.getSimpleName();

    private TypeClassFactory() {}

    public static TypeClass getTypeClass(CompilationUnit compilationUnit) {
        TypeDeclaration<?> type = compilationUnit.getType(0);

        Optional<AnnotationExpr> annotationByClass = type.getAnnotationByClass(Service.class);
        if (annotationByClass.isPresent()) {
            return new ServiceTypeClass(compilationUnit, annotationByClass.get());
        }

        annotationByClass = type.getAnnotationByClass(Component.class);
        if (annotationByClass.isPresent()) {
            Optional<AnnotationExpr> annotationByClassPath = type.getAnnotationByClass(Path.class);
            return new ComponentTypeClass(
                compilationUnit,
                annotationByClass.get(),
                annotationByClassPath.orElse(null)
            );
        }

        annotationByClass = type.getAnnotationByClass(MeatworkConfiguration.class);
        if (annotationByClass.isPresent()) {
            return new MeatworkConfigurationTypeClass(compilationUnit, annotationByClass.get());
        }

        String propertyFullName = ImportUtils.getFullNameByClassName(
            PROPERTY_SIMPLE_NAME,
            compilationUnit.getImports()
        );

        if (type instanceof EnumDeclaration enumDeclaration) {
            if (
                enumDeclaration
                    .getImplementedTypes()
                    .stream()
                    .anyMatch(it -> {
                        boolean match = false;
                        if (it.getName().asString().equals(PROPERTY_SIMPLE_NAME)) {
                            match = Property.class.getName().equals(propertyFullName);
                        }
                        return match;
                    })
            ) {
                return new ModelEntityTypeClass(
                    compilationUnit,
                    null,
                    compilationUnit.getImports()
                );
            }
        }

        return null;
    }
}
