package com.taliro.plugin.scanner.factory.component;

import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.ElementType;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record PathMethodFinder(
    String methodName,
    String fullNameClassToTarget,
    String path,
    String method
)
    implements ElementFinder {
    @Override
    public ElementType getElementType() {
        return ElementType.PATH;
    }
}
