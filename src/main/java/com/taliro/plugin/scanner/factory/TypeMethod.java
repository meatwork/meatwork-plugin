package com.taliro.plugin.scanner.factory;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface TypeMethod {
    ElementFinder getMethodFinder();
}
