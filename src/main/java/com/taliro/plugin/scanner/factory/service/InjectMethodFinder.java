package com.taliro.plugin.scanner.factory.service;

import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.ElementType;

import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record InjectMethodFinder(String methodInjectName, List<String> dependencies) implements ElementFinder {
    @Override
    public ElementType getElementType() {
        return ElementType.INJECT;
    }
}
