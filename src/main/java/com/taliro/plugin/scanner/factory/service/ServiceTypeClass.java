package com.taliro.plugin.scanner.factory.service;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.meatwork.tools.event.Bind;
import com.meatwork.tools.event.Observe;
import com.meatwork.tools.service.Inject;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.factory.AbstractTypeClass;
import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.component.BindTypeMethod;

import java.util.List;

import static com.taliro.plugin.scanner.factory.AnnotationUtils.getConstructorByAnnotation;
import static com.taliro.plugin.scanner.factory.AnnotationUtils.getMethodByAnnotation;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

public class ServiceTypeClass extends AbstractTypeClass {

    public ServiceTypeClass(CompilationUnit compilationUnit,
                            AnnotationExpr annotationExpr) {
        super(
                compilationUnit,
                annotationExpr
        );
    }

    @Override
    public ClassFinder getClassFinder() {

        TypeDeclaration<?> type = compilationUnit.getType(0);
        String interfaceLinked = ((NormalAnnotationExpr) annotationExpr)
                .getPairs()
                .get(0)
                .getValue()
                .toString()
                .replace(".class", "");

        String packageName = compilationUnit
                .getPackageDeclaration()
                .get()
                .getName()
                .toString();

        String className = type
                .getName()
                .getIdentifier();

        ClassFinder classFinder = new ClassFinder(
                ClassFinder.TypeClass.SERVICE,
                packageName,
                className,
                getFullNameClassByClassName(interfaceLinked, packageName)
        );

        String fullCurrentClassName = "%s.%s".formatted(
                packageName,
                className
        );

        getInjectMethod(
                classFinder,
                type
        );

        getObserveMethod(
                classFinder,
                type,
                fullCurrentClassName
        );

        getBindMethod(
                classFinder,
                type,
                fullCurrentClassName,
                compilationUnit.getImports(),
                packageName
        );

        return classFinder;
    }

    private void getBindMethod(ClassFinder classFinder,
                               TypeDeclaration<?> type,
                               String fullCurrentClassName,
                               NodeList<ImportDeclaration> imports,
                               String packageName) {
        List<MethodDeclaration> listObserve = getMethodByAnnotation(
                type,
                Bind.class
        );

        if (!listObserve.isEmpty()) {
            for (MethodDeclaration methodDeclaration : listObserve) {
                BindTypeMethod observeTypeMethod = new BindTypeMethod(
                        methodDeclaration,
                        fullCurrentClassName,
                        imports,
                        packageName

                );
                classFinder.addElement(observeTypeMethod.getMethodFinder());
            }
        }
    }

    private String getFullNameClassByClassName(String className, String packageName) {
        return compilationUnit
                .getImports()
                .stream()
                .filter(it -> it
                        .getName()
                        .toString()
                        .endsWith(className))
                .findFirst()
                .map(it -> it.getName().toString())
                .orElse("%s.%s".formatted(packageName, className));
    }

    private void getInjectMethod(ClassFinder classFinder,
                                         TypeDeclaration<?> type) {

        List<ConstructorDeclaration> constructorInject = getConstructorByAnnotation(
                type,
                Inject.class
        );

        if (constructorInject.size() > 1) {
            throw new IllegalArgumentException("annotation @inject cannot be use multiple in same class !");
        }

        ConstructorDeclaration constructorDeclaration = constructorInject
                .stream()
                .findFirst()
                .orElse(null);

        if (constructorDeclaration == null) {
            return;
        }

        InjectTypeMethod injectTypeMethod = new InjectTypeMethod(constructorDeclaration);
        ElementFinder elementFinder = injectTypeMethod.getMethodFinder();
        classFinder.addElement(elementFinder);
    }

    private void getObserveMethod(ClassFinder classFinder,
                                  TypeDeclaration<?> type,
                                  String fullCurrentClassName) {
        List<MethodDeclaration> listObserve = getMethodByAnnotation(
                type,
                Observe.class
        );

        if (!listObserve.isEmpty()) {
            for (MethodDeclaration methodDeclaration : listObserve) {
                ObserveTypeMethod observeTypeMethod = new ObserveTypeMethod(
                        compilationUnit,
                        methodDeclaration,
                        fullCurrentClassName
                );
                classFinder.addElement(observeTypeMethod.getMethodFinder());
            }
        }
    }

}
