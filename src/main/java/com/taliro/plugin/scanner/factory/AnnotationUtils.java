package com.taliro.plugin.scanner.factory;

import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import java.lang.annotation.Annotation;
import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class AnnotationUtils {

    private AnnotationUtils() {}

    public static List<MethodDeclaration> getMethodByAnnotation(
        TypeDeclaration<?> type,
        Class<? extends Annotation> clazz
    ) {
        return type
            .getMethods()
            .stream()
            .filter(it -> it.getAnnotationByClass(clazz).isPresent())
            .toList();
    }

    public static List<ConstructorDeclaration> getConstructorByAnnotation(
        TypeDeclaration<?> type,
        Class<? extends Annotation> clazz
    ) {
        return type
            .getConstructors()
            .stream()
            .filter(it -> it.getAnnotationByClass(clazz).isPresent())
            .toList();
    }
}
