package com.taliro.plugin.scanner.factory.entity;

import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.EnumConstantDeclaration;
import com.github.javaparser.ast.expr.ClassExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.meatwork.tools.model.Id;
import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.ImportUtils;
import com.taliro.plugin.scanner.factory.TypeMethod;
import com.taliro.plugin.scanner.factory.entity.parser.PropertyFinder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

@RequiredArgsConstructor
public class PropertyTypeMethod implements TypeMethod {

    private final List<String> listEntity;
    private final NodeList<EnumConstantDeclaration> entries;
    private final NodeList<ImportDeclaration> importDeclarations;
    private final String packageName;
    private final String className;

    @Override
    public ElementFinder getMethodFinder() {
        List<PropertyFinder> propertyFinders = entries
            .stream()
            .map(it ->
                new PropertyFinder(
                    it.getName().getIdentifier(),
                    ImportUtils.getFullNameByClassName(className, importDeclarations, packageName),
                    it.isAnnotationPresent(Id.class),
                    getTypeName(it.getArgument(0))
                )
            )
            .toList();

        return new PropertyFinderEntity(
            listEntity
                .stream()
                .map(it -> ImportUtils.getFullNameByClassName(it, importDeclarations, packageName))
                .collect(Collectors.toSet()),
            propertyFinders
        );
    }

    private String getTypeName(Expression expression) {
        if (expression instanceof MethodCallExpr methodCallExpr) {
            return getTypeNameByMethodCall(methodCallExpr);
        } else if (expression instanceof ClassExpr classExpr) {
            return getTypeByClass(classExpr);
        } else {
            throw new IllegalArgumentException(
                "%s type %s is not supported".formatted(
                        expression.toString(),
                        expression.getClass().getName()
                    )
            );
        }
    }

    private String getTypeNameByMethodCall(MethodCallExpr methodCallExpr) {
        List<Node> childNodes = methodCallExpr.getChildNodes();

        TypeParameterValues typeParameterValues = new TypeParameterValues(
            childNodes,
            importDeclarations,
            packageName
        );
        return "%s<%s>".formatted(
                typeParameterValues.getCollectionClass().replace(".class", ""),
                typeParameterValues.getTypeClass().replace(".class", "")
            );
    }

    private String getTypeByClass(ClassExpr classExpr) {
        return classExpr.toString().replace(".class", "");
    }

    @Getter
    static class TypeParameterValues {

        private final String collectionClass;
        private final String typeClass;

        public TypeParameterValues(
            List<Node> childNodes,
            NodeList<ImportDeclaration> importDeclarations,
            String packageName
        ) {
            this.typeClass =
                ImportUtils.getFullNameByClassName(
                    childNodes.get(3).toString(),
                    importDeclarations,
                    packageName
                );
            this.collectionClass =
                ImportUtils.getFullNameByClassName(
                    childNodes.get(2).toString(),
                    importDeclarations
                );
        }
    }
}
