package com.taliro.plugin.scanner.factory;

import com.taliro.plugin.scanner.ClassFinder;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface TypeClass {
    ClassFinder getClassFinder();
}
