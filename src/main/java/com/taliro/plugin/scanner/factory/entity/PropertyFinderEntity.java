package com.taliro.plugin.scanner.factory.entity;

import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.ElementType;
import com.taliro.plugin.scanner.factory.entity.parser.PropertyFinder;

import java.util.Collection;
import java.util.Set;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record PropertyFinderEntity(Set<String> entityLinked, Collection<PropertyFinder> propertyFinders) implements ElementFinder {
	@Override
	public ElementType getElementType() {
		return ElementType.FIELD;
	}
}
