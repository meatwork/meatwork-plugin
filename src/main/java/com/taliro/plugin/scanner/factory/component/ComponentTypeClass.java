package com.taliro.plugin.scanner.factory.component;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.meatwork.tools.event.Bind;
import com.meatwork.tools.event.Observe;
import com.meatwork.tools.http.DELETE;
import com.meatwork.tools.http.GET;
import com.meatwork.tools.http.POST;
import com.meatwork.tools.http.PUT;
import com.meatwork.tools.http.Path;
import com.meatwork.tools.service.Inject;
import com.meatwork.tools.service.Startup;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.factory.AbstractTypeClass;
import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.service.InjectTypeMethod;

import java.lang.annotation.Annotation;
import java.util.List;

import static com.taliro.plugin.scanner.factory.AnnotationUtils.getConstructorByAnnotation;
import static com.taliro.plugin.scanner.factory.AnnotationUtils.getMethodByAnnotation;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

public class ComponentTypeClass extends AbstractTypeClass {

    private final AnnotationExpr annotationPathExpr;

    public ComponentTypeClass(CompilationUnit compilationUnit,
                              AnnotationExpr annotationExpr, AnnotationExpr annotationPathExpr) {
        super(
                compilationUnit,
                annotationExpr
        );
        this.annotationPathExpr = annotationPathExpr;
    }

    @Override
    public ClassFinder getClassFinder() {

        TypeDeclaration<?> type = compilationUnit.getType(0);

        String packageName = compilationUnit
                .getPackageDeclaration()
                .get()
                .getName()
                .toString();
        String className = type
                .getName()
                .getIdentifier();
        ClassFinder classFinder = new ClassFinder(
                ClassFinder.TypeClass.COMPONENT,
                packageName,
                className,
                null
        );

        String fullCurrentClassName = "%s.%s".formatted(
                packageName,
                className
        );

        String pathParent = null;
        if (annotationPathExpr != null) {
            pathParent = ((SingleMemberAnnotationExpr) annotationPathExpr)
                    .getMemberValue()
                    .toString();
        }

        getInjectMethod(
                classFinder,
                type
        );

        getObserveMethod(
                classFinder,
                type,
                fullCurrentClassName
        );

        getStartupMethod(
                classFinder,
                type,
                fullCurrentClassName
        );

        getPathMethod(
                classFinder,
                type,
                fullCurrentClassName,
                pathParent
        );

        getBindMethod(
                classFinder,
                type,
                fullCurrentClassName,
                compilationUnit.getImports(),
                packageName
        );

        return classFinder;
    }

    private void getBindMethod(ClassFinder classFinder,
                               TypeDeclaration<?> type,
                               String fullCurrentClassName,
                               NodeList<ImportDeclaration> imports,
                               String packageName) {
        List<MethodDeclaration> listObserve = getMethodByAnnotation(
                type,
                Bind.class
        );

        if (!listObserve.isEmpty()) {
            for (MethodDeclaration methodDeclaration : listObserve) {
                BindTypeMethod observeTypeMethod = new BindTypeMethod(
                        methodDeclaration,
                        fullCurrentClassName,
                        imports,
                        packageName

                );
                classFinder.addElement(observeTypeMethod.getMethodFinder());
            }
        }
    }

    private void getInjectMethod(ClassFinder classFinder,
                                 TypeDeclaration<?> type) {

        List<ConstructorDeclaration> constructorInject = getConstructorByAnnotation(
                type,
                Inject.class
        );

        if (constructorInject.size() > 1) {
            throw new IllegalArgumentException("annotation @inject cannot be use multiple in same class !");
        }

        ConstructorDeclaration constructorDeclaration = constructorInject
                .stream()
                .findFirst()
                .orElse(null);

        if (constructorDeclaration == null) {
            return;
        }

        InjectTypeMethod injectTypeMethod = new InjectTypeMethod(constructorDeclaration);
        ElementFinder elementFinder = injectTypeMethod.getMethodFinder();
        classFinder.addElement(elementFinder);
    }

    private void getPathMethod(ClassFinder classFinder,
                               TypeDeclaration<?> type,
                               String fullCurrentClassName, String pathParent) {

        List<MethodDeclaration> listObserve = getMethodByAnnotation(
                type,
                Path.class
        );

        if (!listObserve.isEmpty()) {
            for (MethodDeclaration methodDeclaration : listObserve) {
                String methodNameIfExist = getMethodNameIfExist(methodDeclaration);
                PathTypeMethod observeTypeMethod = new PathTypeMethod(
                        methodDeclaration,
                        fullCurrentClassName,
                        pathParent,
                        methodNameIfExist
                );
                classFinder.addElement(observeTypeMethod.getMethodFinder());
            }
        }
    }

    @SuppressWarnings("unchecked")
    private String getMethodNameIfExist(MethodDeclaration methodDeclaration) {
        Class<? extends Annotation>[] list = new Class[]{GET.class, POST.class, PUT.class, DELETE.class};
        for (Class<? extends Annotation> aClass : list) {
            if(methodDeclaration.isAnnotationPresent(aClass)) {
                return aClass.getSimpleName();
            }
        }
        throw new RuntimeException("cannot found methods, with @Path you need to add @GET, @POST, @Put or @Delete");
    }

    private void getStartupMethod(ClassFinder classFinder,
                                  TypeDeclaration<?> type,
                                  String fullCurrentClassName) {
        List<MethodDeclaration> listObserve = getMethodByAnnotation(
                type,
                Startup.class
        );

        if (!listObserve.isEmpty()) {
            for (MethodDeclaration methodDeclaration : listObserve) {
                StartupTypeMethod observeTypeMethod = new StartupTypeMethod(
                        methodDeclaration,
                        fullCurrentClassName
                );
                classFinder.addElement(observeTypeMethod.getMethodFinder());
            }
        }
    }

    private void getObserveMethod(ClassFinder classFinder,
                                  TypeDeclaration<?> type,
                                  String fullCurrentClassName) {
        List<MethodDeclaration> listObserve = getMethodByAnnotation(
                type,
                Observe.class
        );

        if (!listObserve.isEmpty()) {
            for (MethodDeclaration methodDeclaration : listObserve) {
                ObserveTypeMethod observeTypeMethod = new ObserveTypeMethod(
                        compilationUnit,
                        methodDeclaration,
                        fullCurrentClassName
                );
                classFinder.addElement(observeTypeMethod.getMethodFinder());
            }
        }
    }
}
