package com.taliro.plugin.scanner.factory.service;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.TypeMethod;

import java.util.ArrayList;
import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class InjectTypeMethod implements TypeMethod {

    private final ConstructorDeclaration constructorDeclaration;

    public InjectTypeMethod(ConstructorDeclaration constructorDeclaration) {
        this.constructorDeclaration = constructorDeclaration;
    }

    @Override
    public ElementFinder getMethodFinder() {

        String methodName = constructorDeclaration
                .getName()
                .toString();
        NodeList<Parameter> parameters = constructorDeclaration.getParameters();

        List<String> dependencies = new ArrayList<>();
        for (Parameter parameter : parameters) {
            String parameterName = parameter
                    .getType()
                    .toString();
            dependencies.add(parameterName);
        }

        return new InjectMethodFinder(
                methodName,
                dependencies
        );
    }
}
