package com.taliro.plugin.scanner.factory.component;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.meatwork.tools.http.Path;
import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.TypeMethod;
import java.util.Optional;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class PathTypeMethod implements TypeMethod {

    private final MethodDeclaration methodDeclaration;
    private final String fullCurrentClassName;
    private final String path;
    private final String method;

    public PathTypeMethod(
        MethodDeclaration methodDeclaration,
        String fullCurrentClassName,
        String path,
        String method
    ) {
        this.methodDeclaration = methodDeclaration;
        this.fullCurrentClassName = fullCurrentClassName;
        this.path = path;
        this.method = method;
    }

    @Override
    public ElementFinder getMethodFinder() {
        Optional<AnnotationExpr> annotationByClass = methodDeclaration.getAnnotationByClass(
            Path.class
        );
        String pathChildren = "";
        if (annotationByClass.isPresent()) {
            pathChildren =
                ((SingleMemberAnnotationExpr) annotationByClass.get()).getMemberValue().toString();
        }

        String fullPath = removeQuote(path) + removeQuote(pathChildren);

        return new PathMethodFinder(
            methodDeclaration.getName().getIdentifier(),
            fullCurrentClassName,
            fullPath,
            method
        );
    }

    private String removeQuote(String value) {
        return value.replace("\"", "");
    }
}
