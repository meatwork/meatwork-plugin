package com.taliro.plugin.scanner.factory.service;

import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.ElementType;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record ObserveMethodFinder(String methodName, String fullNameClassToTarget, String typeParameterName, int priority) implements ElementFinder {
    @Override
    public ElementType getElementType() {
        return ElementType.OBSERVE;
    }
}
