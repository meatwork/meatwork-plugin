package com.taliro.plugin.scanner.factory.configuration;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.meatwork.tools.configuration.MeatworkConfigurationType;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.factory.AbstractTypeClass;

import java.util.Map;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class MeatworkConfigurationTypeClass extends AbstractTypeClass {

    public MeatworkConfigurationTypeClass(
        CompilationUnit compilationUnit,
        AnnotationExpr annotationExpr
    ) {
        super(compilationUnit, annotationExpr);
    }

    @Override
    public ClassFinder getClassFinder() {
        ClassFinder classFinder = new ClassFinder(
            ClassFinder.TypeClass.CONFIGURATION,
            null,
            null,
            null
        );

        Map<String, Object> configList = Map.of();
        if (annotationExpr
                .getClass()
                .equals(NormalAnnotationExpr.class)) {
            configList = ((NormalAnnotationExpr) annotationExpr)
                    .getPairs()
                    .stream()
                    .collect(
                            Collectors.toMap(
                                    it -> MeatworkConfigurationType
                                            .valueOfName(it
                                                                 .getName()
                                                                 .toString())
                                            .name(),
                                    it -> this.castObject(it.getValue())
                            )
                    );
        }

        addMissingElement(configList);

        classFinder.addAnnotationFinded(configList);
        return classFinder;
    }

    private void addMissingElement(Map<String, Object> configList) {
        MeatworkConfigurationType[] values = MeatworkConfigurationType.values();
        for (MeatworkConfigurationType value : values) {
            if (!configList.containsKey(value.name())) {
                configList.put(value.name(), value.getValue());
            }
        }
    }

    private Object castObject(Object value) {
        if (hasBoolean(value)) {
            return Boolean.valueOf(value.toString());
        } else if (hasNumber(value)) {
            return Integer.parseInt(value.toString());
        } else {
            return value.toString();
        }
    }

    private boolean hasBoolean(Object value) {
        return "true".equals(value.toString()) || "false".equals(value.toString());
    }

    private boolean hasNumber(Object value) {
        return value.toString().matches("-?\\d+");
    }
}
