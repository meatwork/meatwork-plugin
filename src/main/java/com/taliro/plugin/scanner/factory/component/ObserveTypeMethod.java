package com.taliro.plugin.scanner.factory.component;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.meatwork.tools.event.Observe;
import com.taliro.plugin.StringUtils;
import com.taliro.plugin.scanner.factory.ElementFinder;
import com.taliro.plugin.scanner.factory.TypeMethod;
import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ObserveTypeMethod implements TypeMethod {

    private final CompilationUnit compilationUnit;
    private final MethodDeclaration methodDeclaration;
    private final String fullCurrentClassName;

    public ObserveTypeMethod(
        CompilationUnit compilationUnit,
        MethodDeclaration methodDeclaration,
        String fullCurrentClassName
    ) {
        this.compilationUnit = compilationUnit;
        this.methodDeclaration = methodDeclaration;
        this.fullCurrentClassName = fullCurrentClassName;
    }

    @Override
    public ElementFinder getMethodFinder() {
        int priority = 100;
        List<Node> nodes = methodDeclaration
            .getAnnotationByClass(Observe.class)
            .map(AnnotationExpr::getChildNodes)
            .orElse(null);

        if (nodes != null && nodes.size() == 2) {
            Node priorityValue = nodes.get(1);
            priority =
                Integer.parseInt(
                    ((IntegerLiteralExpr) ((MemberValuePair) priorityValue).getValue()).getValue()
                );
        }

        return new ObserveMethodFinder(
            methodDeclaration.getName().getIdentifier(),
            fullCurrentClassName,
            getFullNameEntity(
                compilationUnit,
                StringUtils.capitalizeFirstLetter(
                    methodDeclaration.getParameter(0).getName().getIdentifier()
                )
            ),
            priority
        );
    }

    private String getFullNameEntity(CompilationUnit compilationUnit, String methodName) {
        return compilationUnit
            .getImports()
            .stream()
            .map(it -> it.getName().toString())
            .filter(it -> it.contains(methodName))
            .findFirst()
            .orElse(
                compilationUnit.getPackageDeclaration().get().getName().toString() + methodName
            );
    }
}
