package com.taliro.plugin.scanner.factory.entity;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.meatwork.tools.model.ModelEntity;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.factory.AbstractTypeClass;
import com.taliro.plugin.scanner.factory.ElementFinder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ModelEntityTypeClass extends AbstractTypeClass {

    private final NodeList<ImportDeclaration> imports;

    public ModelEntityTypeClass(
        CompilationUnit compilationUnit,
        AnnotationExpr annotationExpr,
        NodeList<ImportDeclaration> imports
    ) {
        super(compilationUnit, annotationExpr);
        this.imports = imports;
    }

    @Override
    public ClassFinder getClassFinder() {
        TypeDeclaration<?> type = compilationUnit.getType(0);
        Optional<AnnotationExpr> annotationByClass = type.getAnnotationByClass(ModelEntity.class);
        List<String> listEntity = List.of();

        String packageName = compilationUnit.getPackageDeclaration().get().getName().toString();

        String className = type.getName().getIdentifier();

        ClassFinder classFinder = new ClassFinder(
            ClassFinder.TypeClass.ENTITY_PARENT,
            packageName,
            className,
            null
        );

        if (annotationByClass.isPresent()) {
            classFinder.setType(ClassFinder.TypeClass.ENTITY);
            String listEntityString =
                ((NormalAnnotationExpr) annotationByClass.get()).getPairs()
                    .get(0)
                    .getValue()
                    .toString();
            if (listEntityString != null && !listEntityString.isEmpty()) {
                listEntity =
                    Arrays
                        .stream(listEntityString.replace("{", "").replace("}", "").split(","))
                        .map(it -> it.replace(".class", "").trim())
                        .toList();
            }
        }

        ElementFinder methodFinder = new PropertyTypeMethod(
            listEntity,
            ((EnumDeclaration) type).getEntries(),
            imports,
            packageName,
            className
        )
            .getMethodFinder();

        classFinder.addElement(methodFinder);
        return classFinder;
    }
}
