package com.taliro.plugin.scanner.factory;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public enum ElementType {

    OBSERVE,
    STARTUP,
    INJECT,
    PATH,
    BIND,
    FIELD

}
