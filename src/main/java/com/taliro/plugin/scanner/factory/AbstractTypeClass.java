package com.taliro.plugin.scanner.factory;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.AnnotationExpr;
import lombok.RequiredArgsConstructor;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

@RequiredArgsConstructor
public abstract class AbstractTypeClass implements TypeClass {

    protected final CompilationUnit compilationUnit;
    protected final AnnotationExpr annotationExpr;
}
