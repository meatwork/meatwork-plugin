package com.taliro.plugin.scanner;

import com.taliro.plugin.scanner.factory.ElementFinder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

/**
 * The type Class finder.
 */
@Getter
@AllArgsConstructor
public class ClassFinder {

    private final Map<String, Object> annotationFinded = new HashMap<>();

    /**
     * -- GETTER --
     *  Gets type.
     *
     * @return the type
     */
    @Setter
    private TypeClass type;

    /**
     * -- GETTER --
     *  Gets package name.
     *
     * @return the package name
     */
    private final String packageName;

    /**
     * -- GETTER --
     *  Gets simple name.
     *
     * @return the simple name of current class
     */
    private final String simpleName;

    /**
     * -- GETTER --
     *  Gets interface linked.
     *
     * @return the interface linked
     */
    private final String interfaceLinked;

    private final List<ElementFinder> elementFinders = new ArrayList<>();

    /**
     * Add annotation finded.
     *
     * @param valueList the value list
     */
    public void addAnnotationFinded(Map<String, Object> valueList) {
        this.annotationFinded.putAll(valueList);
    }

    /**
     * Add method.
     *
     * @param elementName the method name
     */
    public void addElement(ElementFinder elementName) {
        this.elementFinders.add(elementName);
    }

    /**
     * The enum Type class.
     */
    public enum TypeClass {
        SERVICE,
        COMPONENT,
        CONFIGURATION,
        ENTITY,
        ENTITY_PARENT
    }
}
