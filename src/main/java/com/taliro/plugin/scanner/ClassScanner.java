package com.taliro.plugin.scanner;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ClassScanner {

    private final String path;

    public ClassScanner(String path) {
        this.path = path;
    }

    public CompilationUnit parse() throws IOException {
        File file = new File(this.path);
        JavaParser javaParser = new JavaParser(
            new ParserConfiguration()
                .setLanguageLevel(ParserConfiguration.LanguageLevel.JAVA_8)
                .setCharacterEncoding(StandardCharsets.UTF_8)
        );
        ParseResult<CompilationUnit> parse = javaParser.parse(file);
        return parse
            .getResult()
            .orElseThrow(() ->
                new RuntimeException("cannot be parse class %s".formatted(file.getAbsolutePath()))
            );
    }
}
