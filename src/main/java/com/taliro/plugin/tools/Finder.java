package com.taliro.plugin.tools;

import com.taliro.plugin.DataInitializer;
import com.taliro.plugin.scanner.ClassFinder;
import java.util.regex.Pattern;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Finder {

    public static String getImplFromInterface(
        DataInitializer dataInitializer,
        String interfaceName
    ) {
        return dataInitializer
            .getClassFinders()
            .stream()
            .filter(it ->
                it.getType().equals(ClassFinder.TypeClass.SERVICE) &&
                getClassNameByFullClassName(it.getInterfaceLinked()).equals(interfaceName)
            )
            .map(ClassFinder::getSimpleName)
            .findFirst()
            .orElseThrow(() ->
                new IllegalArgumentException(
                    "cannot find impl from interface %s".formatted(interfaceName)
                )
            );
    }

    private static String getClassNameByFullClassName(String fullClassName) {
        String[] split = fullClassName.split(Pattern.quote("."));
        return split[split.length - 1];
    }
}
