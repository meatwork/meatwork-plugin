package com.taliro.plugin;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class FileUtils {

    private static final String OS = System.getProperty("os.name").toLowerCase();

    private FileUtils() {}

    public static boolean hasFolderExistOrCreateIt(String fileName) {
        String[] split = fileName.split("\\\\|/");
        Path path = Paths.get("");
        int index = 0;
        if (isWindows()) {
            for (String string : split) {
                if (index == 0) {
                    path = path.resolve(string + "\\");
                } else {
                    path = path.resolve(string);
                }
                index++;
            }
        } else {
            path = Paths.get(fileName);
        }

        File file = path.toFile();

        if (!file.exists()) {
            return file.mkdirs();
        }
        return true;
    }

    public static boolean isWindows() {
        return (OS.contains("win"));
    }
}
