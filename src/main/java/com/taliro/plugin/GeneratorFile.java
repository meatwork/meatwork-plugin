package com.taliro.plugin;

import com.taliro.plugin.generator.AbstractGenerator;
import com.taliro.plugin.generator.ApplicationGenerator;
import com.taliro.plugin.generator.BindRegistryGenerator;
import com.taliro.plugin.generator.ClassComponentGenerator;
import com.taliro.plugin.generator.ClassServiceGenerator;
import com.taliro.plugin.generator.ConfigurationGenerator;
import com.taliro.plugin.generator.EntityGenerator;
import com.taliro.plugin.generator.EntityRegistryGenerator;
import com.taliro.plugin.generator.EntityRepositoryGenerator;
import com.taliro.plugin.generator.EventRegistryGenerator;
import com.taliro.plugin.generator.RouterRegistryGenerator;
import com.taliro.plugin.generator.ServiceRegistryGenerator;
import com.taliro.plugin.generator.StartupRegistryGenerator;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class GeneratorFile {

    private static final List<Function<DataInitializer, AbstractGenerator>> generateFiles = new LinkedList<>();

    static {
        generateFiles.add(ClassComponentGenerator::new);
        generateFiles.add(ClassServiceGenerator::new);
        generateFiles.add(EventRegistryGenerator::new);
        generateFiles.add(RouterRegistryGenerator::new);
        generateFiles.add(ServiceRegistryGenerator::new);
        generateFiles.add(StartupRegistryGenerator::new);
        generateFiles.add(BindRegistryGenerator::new);
        generateFiles.add(ConfigurationGenerator::new);
        generateFiles.add(EntityGenerator::new);
        generateFiles.add(EntityRepositoryGenerator::new);
        generateFiles.add(EntityRegistryGenerator::new);
        generateFiles.add(ApplicationGenerator::new);
    }

    public static void generate(InputConfiguration inputConfiguration)
        throws IOException, URISyntaxException {
        DataInitializer dataInitializer = new DataInitializer(inputConfiguration);

        for (Function<DataInitializer, AbstractGenerator> generateFile : generateFiles) {
            generateFile.apply(dataInitializer).generate();
        }
    }
}
