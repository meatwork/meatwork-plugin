package com.taliro.test.classtest;

import com.meatwork.tools.service.Inject;
import com.meatwork.tools.service.Service;
import com.taliro.test.interfaces.IAnOtherService;

@Service(interfaces = IAnService.class )
public class AnService implements IAnService {

    private IAnOtherService iAnOtherService;

    @Inject
    public AnService(IAnOtherService iAnOtherService) {
        this.iAnOtherService = iAnOtherService;
    }
}
