package com.taliro.test.classtest;

import com.meatwork.tools.event.Observe;
import com.taliro.test.Fight;

public class AnEvent {

    @Observe
    public void onFight(Fight fight) {
        System.out.println(fight.toString());
    }

}
