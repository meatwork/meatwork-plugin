package com.taliro.test.classtest;

import com.meatwork.tools.service.Service;
import com.taliro.test.interfaces.IAnOtherService;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

@Service(interfaces = IAnOtherService.class )
public class AnOtherService implements IAnOtherService {
}
