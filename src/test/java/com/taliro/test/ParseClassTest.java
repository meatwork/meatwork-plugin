package com.taliro.test;

import com.github.javaparser.ast.CompilationUnit;
import com.taliro.plugin.scanner.ClassFinder;
import com.taliro.plugin.scanner.ClassParser;
import com.taliro.plugin.scanner.ClassScanner;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ParseClassTest {

    @AfterAll
    public static void destroy() throws IOException {
        Path path = Paths.get(System.getProperty("user.dir"));
        Path pathProject = path.resolve("target/generated-sources/ivent/injector");
        FileUtils.deleteDirectory(pathProject.toFile());
    }

    @Test
    @Disabled
    void loadParserByTemplate() throws IOException {
        Path path = Paths.get(System.getProperty("user.dir"));
        Path pathProject = path.resolve("src/test/java/com/taliro/test/classtest");
        List<File> javaFiles = findJavaFiles(pathProject.toFile());

        for (File javaFile : javaFiles) {
            CompilationUnit parse = new ClassScanner(javaFile.getAbsolutePath()).parse();
            ClassFinder classFinder = new ClassParser(parse).build();
        }
    }


    private static List<File> findJavaFiles(File directory) {
        List<File> javaFiles = new ArrayList<>();

        if (directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        // Récursivement, recherchez les fichiers dans les sous-dossiers
                        javaFiles.addAll(findJavaFiles(file));
                    } else if (file.getName().toLowerCase().endsWith(".java")) {
                        // Ajoutez le fichier Java à la liste
                        javaFiles.add(file);
                    }
                }
            }
        }

        return javaFiles;
    }
}
