# Version latest

[![](https://jitpack.io/v/com.gitlab.meatwork/meatwork-plugin.svg)](https://jitpack.io/#com.gitlab.meatwork/meatwork-plugin)


# installation plugin 

## Ajouter jitpack

```xml
<pluginRepositories>
    <pluginRepository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </pluginRepository>
</pluginRepositories>
 <repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://www.jitpack.io</url>
    </repository>
</repositories>
```

## Ajouter le plugin

```xml
<plugins>
    <plugin>
        <groupId>com.gitlab.meatwork</groupId>
        <artifactId>meatwork-plugin</artifactId>
        <version>${version}</version>
        <executions>
            <execution>
                <phase>generate-sources</phase>
                <goals>
                    <goal>class-generate</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
            <source>21</source>
            <target>21</target>
            <includes>
                <include>generated-sources/**</include>
            </includes>
        </configuration>
    </plugin>
</plugins>
```

# Installation tools

```xml
<dependency>
    <groupId>com.gitlab.meatwork</groupId>
    <artifactId>meatwork-tools</artifactId>
    <version>${version}</version>
</dependency>
```

# Configuration

Enable Http Server : add to main class ``@MeatworkConfiguration(enableHttpServer = true, portHttpServer = 8080)``

> portHttpServer is Optional !